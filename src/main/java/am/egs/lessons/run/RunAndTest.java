package am.egs.lessons.run;

/**
 * Created by haykh on 5/17/2019.
 */

import java.util.List;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import am.egs.lessons.dto.PersonDTO;
import am.egs.lessons.configuration.ApplicationConfig;
import am.egs.lessons.services.PersonService;

public class RunAndTest {

  public static void main(String args[]) {

    AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
    PersonService personService = (PersonService) context.getBean("personService");

    PersonDTO yashwant = new PersonDTO(1, "Yashwant", "Chavan", 32);
    PersonDTO mahesh = new PersonDTO(2, "Mahesh", "Patil", 25);
    PersonDTO vishal = new PersonDTO(3, "Vishal", "Naik", 40);

    personService.addPerson(yashwant);
    personService.addPerson(mahesh);
    personService.addPerson(vishal);

    System.out.println("Find All");
    List <PersonDTO> persons = personService.findAll();
    for (PersonDTO person: persons) {
      System.out.println(person);
    }

    System.out.println("Delete person Id = 3");
    int deleteMe = 3;
    personService.deletePerson(deleteMe);

    yashwant.setFirstName("Yashwant - Updated");
    yashwant.setLastName("Chavan - Updated");
    yashwant.setAge(40);

    System.out.println("Update person Id = 1");
    int updateMe = 1;
    personService.editPerson(yashwant, updateMe);

    System.out.println("Find person Id = 2");
    PersonDTO person = personService.find(2);
    System.out.println(person);

    System.out.println("Find All Again");
    persons = personService.findAll();
    for (PersonDTO p: persons) {
      System.out.println(p);
    }

    context.close();
  }

}